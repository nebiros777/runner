// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TileMiddleGroundDefault.generated.h"

UCLASS()
class RUNNER_API ATileMiddleGroundDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileMiddleGroundDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* LeftField = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* RightField = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* Connection = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UBoxComponent* Trigger = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UInstancedStaticMeshComponent* FirstObjectOnTheGround = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UInstancedStaticMeshComponent* SecondObjectOnTheGround = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UInstancedStaticMeshComponent* ThirdObjectOnTheGround = nullptr;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void DestroyTick(float DeltaTime);
	UFUNCTION()
		void TriggerBeginOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void SetNextTileIsBuilt();

	UFUNCTION()
	void RandomSpawnDecor(UStaticMeshComponent* CurrentComponent, UInstancedStaticMeshComponent* CurrentObject);

	UFUNCTION()
		FVector4 GetFloatsFromBound(UStaticMeshComponent* CurrentComponent);

	UPROPERTY()
		bool bIsNextTileBuilt = false;
	UPROPERTY()
		float DestroyTime = 3.0f;
	UPROPERTY()
		float DestroyTimer = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enviroment in fields")
		int32 MinRange = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enviroment in fields")
		int32 MaxRange = 20;

};
