// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBaseDefault.h"
#include "Components/StaticMeshComponent.h"
//#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "RunnerGameMode.h"
#include "RunnerCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABonusBaseDefault::ABonusBaseDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	BonusMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tile floor"));
	BonusMesh->SetGenerateOverlapEvents(false);
	BonusMesh->SetCollisionProfileName(TEXT("Custom"));
	BonusMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	BonusMesh->SetupAttachment(RootComponent);

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	Trigger->SetBoxExtent(FVector(100.f, 500.0f, 300.f));
	Trigger->SetupAttachment(RootComponent);

	BonusFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bonus FX"));
	BonusFX->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABonusBaseDefault::BeginPlay()
{
	Super::BeginPlay();
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &ABonusBaseDefault::TriggerBeginOverlap);

	
}

// Called every frame
void ABonusBaseDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusBaseDefault::TriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	
	if (OtherActor)
	{
		ARunnerCharacter* myCharacter = Cast<ARunnerCharacter>(OtherActor);
		if (myCharacter)
		{
			ARunnerGameMode* GM = Cast<ARunnerGameMode>(GetWorld()->GetAuthGameMode());
			if (GM)
			{
				GM->Coins++;
				if (GM->BonusGetFX)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GM->BonusGetFX, BonusMesh->GetComponentTransform());
				}
			}

			/* ToDo Play Sound*/

			this->Destroy();
		}
	}
}

