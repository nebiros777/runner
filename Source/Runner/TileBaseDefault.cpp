// Fill out your copyright notice in the Description page of Project Settings.


#include "TileBaseDefault.h"
#include "RunnerGameMode.h"
#include "RunnerCharacter.h"
#include "Math/UnrealMathUtility.h"
#include "ObstacleBaseDefault.h"
#include "BonusBaseDefault.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values
ATileBaseDefault::ATileBaseDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	TileFloor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tile floor"));
	TileFloor->SetGenerateOverlapEvents(false);
	/*MagazineDrop->SetCollisionProfileName(TEXT("Custom"));
	MagazineDrop->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	MagazineDrop->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	MagazineDrop->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	MagazineDrop->SetEnableGravity(true);
	MagazineDrop->SetSimulatePhysics(true);*/
	TileFloor->SetupAttachment(RootComponent);

	Connection = CreateDefaultSubobject<UArrowComponent>(TEXT("Connection"));
	Connection->SetupAttachment(RootComponent);

	Line0 = CreateDefaultSubobject<UArrowComponent>(TEXT("Line0"));
	Line0->SetRelativeLocation(FVector(0.0f, -250.0f, 0.0f));
	Line0->SetupAttachment(RootComponent);

	Line1 = CreateDefaultSubobject<UArrowComponent>(TEXT("Line1"));
	Line1->SetupAttachment(RootComponent);

	Line2 = CreateDefaultSubobject<UArrowComponent>(TEXT("Line2"));
	Line2->SetRelativeLocation(FVector(0.0f, 250.0f, 0.0f));
	Line2->SetupAttachment(RootComponent);

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));	
	Trigger->SetRelativeLocation(FVector(400.f,0.0f,300.f));
	Trigger->SetBoxExtent(FVector(100.f, 500.0f, 300.f));
	Trigger->SetupAttachment(RootComponent);
	
	

}

// Called when the game starts or when spawned
void ATileBaseDefault::BeginPlay()
{
	Super::BeginPlay();
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &ATileBaseDefault::TriggerBeginOverlap);
	
	SpawnObstacle();
}

// Called every frame
void ATileBaseDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DestroyTick(DeltaTime);

}

void ATileBaseDefault::DestroyTick(float DeltaTime)
{
	if (bIsNextTileBuilt)
	{
		if (DestroyTimer >= DestroyTime)
		{
			this->Destroy();
		}
		else
		{
			DestroyTimer += DeltaTime;
		}
	}
}

void ATileBaseDefault::TriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		ARunnerCharacter* myCharacter = Cast<ARunnerCharacter>(OtherActor);
		if (myCharacter && !bIsNextTileBuilt)
		{
			ARunnerGameMode* GM = Cast<ARunnerGameMode>(GetWorld()->GetAuthGameMode());
			if (GM)
			{
				GM->SpawnTile(GM->InClassFirst);
				SetNextTileIsBuilt();
				GM->CurrentPoints += GM->PointsMultiplier * 1;
			}
			UCharacterMovementComponent* MovementComp = Cast<UCharacterMovementComponent>(myCharacter->GetCharacterMovement());
			MovementComp->MaxWalkSpeed *= 1.05;
		}
	}

}

void ATileBaseDefault::SetNextTileIsBuilt()
{
	if (!bIsNextTileBuilt)
	{
		bIsNextTileBuilt = true;
	}
}

void ATileBaseDefault::SpawnObstacle()
{
	RandomSpawnObstacle(Line0->GetComponentTransform());
	RandomSpawnObstacle(Line1->GetComponentTransform());
	RandomSpawnObstacle(Line2->GetComponentTransform());
}

void ATileBaseDefault::RandomSpawnObstacle(FTransform Transform)
{
	//FVector SpawnLocation 
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();

	int32 RandomID = FMath::RandRange(0, MaxRandomObstacleID);
	
	ARunnerGameMode* GM = Cast<ARunnerGameMode>(GetWorld()->GetAuthGameMode());
	//FVector SpawnLocationTransform.GetLocation();

	switch (RandomID)
	{
	case 0:
	{break; }
	case 1: {
		AObstacleBaseDefault* Obstacle00 = Cast<AObstacleBaseDefault>(GetWorld()->SpawnActor(GM->Obstacle0, &Transform, SpawnParams));
		Obstacles.AddUnique(Obstacle00);
		break;
	}
	case 2: {
		AObstacleBaseDefault* Obstacle01 = Cast<AObstacleBaseDefault>(GetWorld()->SpawnActor(GM->Obstacle1, &Transform, SpawnParams));
		Obstacles.AddUnique(Obstacle01);
		break;
	}
	case 3: {
		AObstacleBaseDefault* Obstacle02 = Cast<AObstacleBaseDefault>(GetWorld()->SpawnActor(GM->Obstacle2, &Transform, SpawnParams));
		Obstacles.AddUnique(Obstacle02);
		break;
	}
	case 4:
		ABonusBaseDefault* Bonus = Cast<ABonusBaseDefault>(GetWorld()->SpawnActor(GM->Bonus, &Transform, SpawnParams));
		Obstacles.AddUnique(Bonus);

	{break; }

	}
}

