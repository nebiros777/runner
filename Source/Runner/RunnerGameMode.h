// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TileBaseDefault.h"
#include "ObstacleBaseDefault.h"
#include "TileMiddleGroundDefault.h"
#include "RunnerGameMode.generated.h"

UCLASS(minimalapi)
class ARunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	ARunnerGameMode();

	

	UFUNCTION(BlueprintCallable)
		void SpawnTile(TSubclassOf<class ATileBaseDefault> SpawnClass);

	UFUNCTION(BlueprintCallable)
		void SpawnMiddleGroundTile(TSubclassOf<class ATileMiddleGroundDefault> SpawnClass);

	UPROPERTY()
		FVector SpawnLocation = FVector(0.0f);
	UPROPERTY()
		FRotator SpawnRotation = FRotator(0.0f);

	UPROPERTY()
		FVector MiddleGroundSpawnLocation = FVector(0.0f);
	UPROPERTY()
		FRotator MiddleGroundSpawnRotation = FRotator(0.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
		TSubclassOf<class ATileBaseDefault> InClassFirst = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
		TSubclassOf<class ATileBaseDefault> InClassGate = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
		TSubclassOf<class ATileMiddleGroundDefault> InClassMiddleGround = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
		TSubclassOf<class AObstacleBaseDefault> Obstacle0 = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
		TSubclassOf<class AObstacleBaseDefault> Obstacle1 = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
		TSubclassOf<class AObstacleBaseDefault> Obstacle2 = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		UParticleSystem* BonusGetFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		TSubclassOf<class ABonusBaseDefault> Bonus = nullptr;




	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
		int32 CurrentPoints = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
		int32 PointsMultiplier = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
		int32 Coins = 0;

	
};



