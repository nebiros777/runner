// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunnerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterDeath);

UCLASS(config=Game)
class ARunnerCharacter : public ACharacter
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class URotatingMovementComponent* RotMove;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* DeathFXTransform;


	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ARunnerCharacter();

	FOnCharacterDeath OnCharacterDeath;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bool variables")
		bool bIsDead = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bool variables")
		bool bIsRunStarted = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeathFX")
		UParticleSystem* DeathFX = nullptr;
	UPROPERTY()
		int32 Line = 1;
	UPROPERTY()
		int32 NewLine = 1;
	UPROPERTY()
		float LinesY[3];

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface
	virtual void BeginPlay() override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Jump() override;

	UFUNCTION()
		void RunTick(float DeltaTime);

	UFUNCTION()
		bool ActionsValidCheck();

	UFUNCTION()
		void Lower();

	UFUNCTION(BlueprintCallable)
		void Death();

	UFUNCTION()
		void StartRun();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ticks")
		float StartTime = 3.0f;
	UPROPERTY()
		float StartTimer = 0.0f;
	UFUNCTION()
		void StartTick(float DeltaTime);

	UFUNCTION()
		void DeathTickBeforePause(float DeltaTime);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ticks")
		float DeathTime = 1.4f;
	UPROPERTY()
		float DeathTimer = 0.0f;
	UPROPERTY()
		FTimerHandle MemberTimerHandle;

	UFUNCTION()
		void MovingLeft();
	UFUNCTION()
		void MovingRight();
	UFUNCTION()
		void LineTick(float DeltaTime);
	UFUNCTION()
		void GoToCorrectLine();


};

