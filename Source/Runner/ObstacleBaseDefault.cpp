// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleBaseDefault.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "RunnerCharacter.h"

// Sets default values
AObstacleBaseDefault::AObstacleBaseDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tile floor"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetupAttachment(RootComponent);

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	//Trigger->SetRelativeLocation(FVector(400.f, 0.0f, 300.f));
	Trigger->SetBoxExtent(FVector(60.f, 60.0f, 60.f));
	Trigger->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AObstacleBaseDefault::BeginPlay()
{
	Super::BeginPlay();
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &AObstacleBaseDefault::TriggerBeginOverlap);
	
}

// Called every frame
void AObstacleBaseDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AObstacleBaseDefault::TriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		ARunnerCharacter* myCharacter = Cast<ARunnerCharacter>(OtherActor);
		if (myCharacter)
		{
			myCharacter->Death();
		}
		
	}
}

