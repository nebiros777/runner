// Copyright Epic Games, Inc. All Rights Reserved.

#include "RunnerCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"


//////////////////////////////////////////////////////////////////////////
// ARunnerCharacter

ARunnerCharacter::ARunnerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	RotMove = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotationMovement"));
	RotMove->RotationRate.Yaw = 135;
	RotMove->bAutoActivate = false;

	DeathFXTransform = CreateDefaultSubobject<UArrowComponent>(TEXT("DeathFXTransform"));
	DeathFXTransform->SetupAttachment(RootComponent);
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetMesh());
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	LinesY[0] = -250.0f;
	LinesY[1] = 0.0f;
	LinesY[2] = 250.0f;
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void ARunnerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ARunnerCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Lower", IE_Pressed, this, &ARunnerCharacter::Lower);

	PlayerInputComponent->BindAction("MovingLeft", IE_Pressed, this, &ARunnerCharacter::MovingLeft);
	PlayerInputComponent->BindAction("MovingRight", IE_Pressed, this, &ARunnerCharacter::MovingRight);

	PlayerInputComponent->BindAxis("MoveForward", this, &ARunnerCharacter::MoveForward);
	//PlayerInputComponent->BindAxis("MoveRight", this, &ARunnerCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	/*PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ARunnerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ARunnerCharacter::LookUpAtRate);
	*/
	// handle touch devices
	//PlayerInputComponent->BindTouch(IE_Pressed, this, &ARunnerCharacter::TouchStarted);
	//PlayerInputComponent->BindTouch(IE_Released, this, &ARunnerCharacter::TouchStopped);

	// VR headset functionality
	//PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ARunnerCharacter::OnResetVR);
}

void ARunnerCharacter::BeginPlay()
{
	Super::BeginPlay();

	

	UGameplayStatics::GetPlayerController(GetWorld(), 0)->DisableInput(GetWorld()->GetFirstPlayerController());
	
	RotMove->Activate();

}

void ARunnerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	StartTick(DeltaTime);
	RunTick(DeltaTime);
	DeathTickBeforePause(DeltaTime);
	//LineTick(DeltaTime);

}

void ARunnerCharacter::Jump()
{
	if (ActionsValidCheck())
	{
		bPressedJump = true;
		JumpKeyHoldTime = 0.0f;
	}
}

void ARunnerCharacter::RunTick(float DeltaTime)
{

	MoveForward(1.0f);
}

bool ARunnerCharacter::ActionsValidCheck()
{
	if (!bIsDead && bIsRunStarted)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

void ARunnerCharacter::Lower()
{
	if (ActionsValidCheck())
	{
		StopJumping();
		Cast<UCharacterMovementComponent>(this->GetMovementComponent())->AddImpulse(FVector(0.0f, 0.0f, -1000.0f),true);
	}
}

void ARunnerCharacter::Death()
{
	bIsDead = true;
	if (DeathFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathFX, DeathFXTransform->GetComponentTransform());
	}
	RotMove->Activate();

	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0.5f);

	OnCharacterDeath.Broadcast();

}

void ARunnerCharacter::StartRun()
{
	
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->EnableInput(GetWorld()->GetFirstPlayerController());
		bIsRunStarted = true;
		RotMove->Deactivate();
		RotMove->RotationRate.Yaw = 130;

}

void ARunnerCharacter::StartTick(float DeltaTime)
{
	if (!bIsRunStarted && !bIsDead)
	{
		if (StartTimer >= StartTime)
		{
			StartRun();
		}
		else
		{
			StartTimer += DeltaTime;
		}
	}
}

void ARunnerCharacter::DeathTickBeforePause(float DeltaTime)
{
	if (bIsDead)
	{
		if (DeathTimer >= DeathTime)
		{
			UGameplayStatics::SetGamePaused(GetWorld(), true);
		}
		else
		{
			DeathTimer+= DeltaTime;
		}
	}
}

void ARunnerCharacter::MovingLeft()
{
	if (ActionsValidCheck())
	{
		NewLine = FMath::Clamp(Line - 1, 0, 2);
		
		if (Line != NewLine)
		{

			GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ARunnerCharacter::GoToCorrectLine, 0.01f, true, 0.1f);
		}
	}

}

void ARunnerCharacter::MovingRight()
{
	if (ActionsValidCheck())
	{
		NewLine = FMath::Clamp(Line + 1, 0, 2);
		
		if (Line != NewLine)
		{
			
			GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ARunnerCharacter::GoToCorrectLine, 0.01f, true, 0.1f);
		}
	}
}

void ARunnerCharacter::LineTick(float DeltaTime)
{
	
	
	

	if (Line != NewLine)
	{
		if (UKismetMathLibrary::Abs(GetCapsuleComponent()->GetComponentLocation().Y - LinesY[NewLine]) > 10 )
		{
			float NewY = UKismetMathLibrary::FInterpTo(GetCapsuleComponent()->GetComponentLocation().Y, LinesY[NewLine], DeltaTime, 10);
			FVector NewLocation(GetCapsuleComponent()->GetComponentLocation().X, NewY, GetCapsuleComponent()->GetComponentLocation().Z);
			GetCapsuleComponent()->SetWorldLocation(NewLocation);
		}
		else
		{
			Line = NewLine;
		}

	}
}


void ARunnerCharacter::GoToCorrectLine()
{
	if (UKismetMathLibrary::Abs(GetCapsuleComponent()->GetComponentLocation().Y - LinesY[NewLine]) > 10)
	{
		float NewY = UKismetMathLibrary::FInterpTo(GetCapsuleComponent()->GetComponentLocation().Y, LinesY[NewLine], 0.01f, 10);
		FVector NewLocation(GetCapsuleComponent()->GetComponentLocation().X, NewY, GetCapsuleComponent()->GetComponentLocation().Z);
		GetCapsuleComponent()->SetWorldLocation(NewLocation);
	}
	else
	{
		Line = NewLine;
		GetWorldTimerManager().ClearTimer(MemberTimerHandle);
	}
}

void ARunnerCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ARunnerCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ARunnerCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ARunnerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ARunnerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ARunnerCharacter::MoveForward(float Value)
{
	

	if ((Controller != NULL) && (Value != 0.0f) && ActionsValidCheck())
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ARunnerCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
