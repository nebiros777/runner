// Copyright Epic Games, Inc. All Rights Reserved.

#include "RunnerGameMode.h"
#include "RunnerCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARunnerGameMode::ARunnerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ARunnerGameMode::BeginPlay()
{
	Super::BeginPlay();

	SpawnTile(InClassGate);

	for (int i = 0; i < 11; i++)
	{
		SpawnTile(InClassFirst);
	}

	for (int i = 0; i < 2; i++)
	{
		SpawnMiddleGroundTile(InClassMiddleGround);
	}


}

void ARunnerGameMode::SpawnTile(TSubclassOf<class ATileBaseDefault> SpawnClass)
{
	

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	

	ATileBaseDefault* InClass = Cast<ATileBaseDefault>(GetWorld()->SpawnActor(SpawnClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (InClass)
		{
			SpawnLocation = InClass->Connection->GetComponentLocation();
			SpawnRotation = InClass->Connection->GetComponentRotation();
			
		}
	
}

void ARunnerGameMode::SpawnMiddleGroundTile(TSubclassOf<class ATileMiddleGroundDefault> SpawnClass)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();


	ATileMiddleGroundDefault* InClass = Cast<ATileMiddleGroundDefault>(GetWorld()->SpawnActor(SpawnClass, &MiddleGroundSpawnLocation, &MiddleGroundSpawnRotation, SpawnParams));
	if (InClass)
	{
		MiddleGroundSpawnLocation = InClass->Connection->GetComponentLocation();
		MiddleGroundSpawnRotation = InClass->Connection->GetComponentRotation();

	}
}
