// Fill out your copyright notice in the Description page of Project Settings.


#include "TileMiddleGroundDefault.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "RunnerGameMode.h"
#include "RunnerCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "GenericPlatform/GenericPlatformMath.h"


// Sets default values
ATileMiddleGroundDefault::ATileMiddleGroundDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	LeftField = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tile floor Left"));
	LeftField->SetGenerateOverlapEvents(false);
	LeftField->SetupAttachment(RootComponent);

	RightField = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tile floor Right"));
	RightField->SetGenerateOverlapEvents(false);
	RightField->SetupAttachment(RootComponent);

	Connection = CreateDefaultSubobject<UArrowComponent>(TEXT("Connection"));
	Connection->SetupAttachment(RootComponent);

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	Trigger->SetRelativeLocation(FVector(400.f, 0.0f, 300.f));
	Trigger->SetBoxExtent(FVector(100.f, 500.0f, 300.f));
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &ATileMiddleGroundDefault::TriggerBeginOverlap);
	Trigger->SetupAttachment(RootComponent);

	FirstObjectOnTheGround = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("FirstObjectOnTheGround"));
	FirstObjectOnTheGround->SetupAttachment(RootComponent);

	SecondObjectOnTheGround = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("SecondObjectOnTheGround"));
	SecondObjectOnTheGround->SetupAttachment(RootComponent);

	ThirdObjectOnTheGround = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("ThirdObjectOnTheGround"));
	ThirdObjectOnTheGround->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATileMiddleGroundDefault::BeginPlay()
{
	Super::BeginPlay();

	RandomSpawnDecor(LeftField,FirstObjectOnTheGround);
	RandomSpawnDecor(RightField, FirstObjectOnTheGround);

	RandomSpawnDecor(LeftField, SecondObjectOnTheGround);
	RandomSpawnDecor(RightField, SecondObjectOnTheGround);

	RandomSpawnDecor(LeftField, ThirdObjectOnTheGround);
	RandomSpawnDecor(RightField, ThirdObjectOnTheGround);


}

// Called every frame
void ATileMiddleGroundDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DestroyTick(DeltaTime);

}

void ATileMiddleGroundDefault::DestroyTick(float DeltaTime)
{
	if (bIsNextTileBuilt)
	{
		if (DestroyTimer >= DestroyTime)
		{
			this->Destroy();
		}
		else
		{
			DestroyTimer += DeltaTime;
		}
	}
}

void ATileMiddleGroundDefault::TriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		ARunnerCharacter* myCharacter = Cast<ARunnerCharacter>(OtherActor);
		if (myCharacter && !bIsNextTileBuilt)
		{
			ARunnerGameMode* GM = Cast<ARunnerGameMode>(GetWorld()->GetAuthGameMode());
			if (GM)
			{
				GM->SpawnMiddleGroundTile(GM->InClassMiddleGround);
				SetNextTileIsBuilt();
			}
			
		}
	}
}

void ATileMiddleGroundDefault::SetNextTileIsBuilt()
{
	if (!bIsNextTileBuilt)
	{
		bIsNextTileBuilt = true;
	}
}

void ATileMiddleGroundDefault::RandomSpawnDecor(UStaticMeshComponent* CurrentComponent, UInstancedStaticMeshComponent* CurrentObject)
{

	for (int8 i = 0; i < UKismetMathLibrary::RandomIntegerInRange(MinRange,MaxRange); i++)
	{

		FVector4 InFloats = GetFloatsFromBound(CurrentComponent);		
		
		FTransform SpawnTransform;
		SpawnTransform.SetLocation(FVector(InFloats.X,InFloats.Y,0.0f));
		SpawnTransform.SetRotation(UKismetMathLibrary::Quat_MakeFromEuler(FVector(0, 0, InFloats.Z)));
		SpawnTransform.SetScale3D(FVector(InFloats.W, InFloats.W, InFloats.W));
		CurrentObject->AddInstance(SpawnTransform);
		
		
	}
}

FVector4 ATileMiddleGroundDefault::GetFloatsFromBound(UStaticMeshComponent* CurrentComponent)
{
	FVector4 result(0,0,0,0);
	if (CurrentComponent)
	{
		FBoxSphereBounds ComponentBounds = CurrentComponent->Bounds;
		FVector NewBoxExtent = FVector(ComponentBounds.BoxExtent.X, ComponentBounds.BoxExtent.Y * 0.9f, ComponentBounds.BoxExtent.Z);
		FVector RandomPoint = UKismetMathLibrary::RandomPointInBoundingBox(ComponentBounds.Origin, NewBoxExtent);
		result.X = RandomPoint.X - this->GetActorLocation().X;
		result.Y = RandomPoint.Y;
		result.Z = UKismetMathLibrary::RandomFloatInRange(0.0f, 359.0f);
		result.W = UKismetMathLibrary::RandomFloatInRange(0.8f, 1.2f);
	}

	return result;
}

