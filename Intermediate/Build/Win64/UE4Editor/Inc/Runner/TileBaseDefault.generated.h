// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTransform;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef RUNNER_TileBaseDefault_generated_h
#error "TileBaseDefault.generated.h already included, missing '#pragma once' in TileBaseDefault.h"
#endif
#define RUNNER_TileBaseDefault_generated_h

#define Runner_Source_Runner_TileBaseDefault_h_17_SPARSE_DATA
#define Runner_Source_Runner_TileBaseDefault_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRandomSpawnObstacle); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execSetNextTileIsBuilt); \
	DECLARE_FUNCTION(execTriggerBeginOverlap); \
	DECLARE_FUNCTION(execDestroyTick);


#define Runner_Source_Runner_TileBaseDefault_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRandomSpawnObstacle); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execSetNextTileIsBuilt); \
	DECLARE_FUNCTION(execTriggerBeginOverlap); \
	DECLARE_FUNCTION(execDestroyTick);


#define Runner_Source_Runner_TileBaseDefault_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATileBaseDefault(); \
	friend struct Z_Construct_UClass_ATileBaseDefault_Statics; \
public: \
	DECLARE_CLASS(ATileBaseDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ATileBaseDefault)


#define Runner_Source_Runner_TileBaseDefault_h_17_INCLASS \
private: \
	static void StaticRegisterNativesATileBaseDefault(); \
	friend struct Z_Construct_UClass_ATileBaseDefault_Statics; \
public: \
	DECLARE_CLASS(ATileBaseDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ATileBaseDefault)


#define Runner_Source_Runner_TileBaseDefault_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATileBaseDefault(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATileBaseDefault) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATileBaseDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATileBaseDefault); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATileBaseDefault(ATileBaseDefault&&); \
	NO_API ATileBaseDefault(const ATileBaseDefault&); \
public:


#define Runner_Source_Runner_TileBaseDefault_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATileBaseDefault(ATileBaseDefault&&); \
	NO_API ATileBaseDefault(const ATileBaseDefault&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATileBaseDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATileBaseDefault); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATileBaseDefault)


#define Runner_Source_Runner_TileBaseDefault_h_17_PRIVATE_PROPERTY_OFFSET
#define Runner_Source_Runner_TileBaseDefault_h_14_PROLOG
#define Runner_Source_Runner_TileBaseDefault_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_TileBaseDefault_h_17_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_TileBaseDefault_h_17_SPARSE_DATA \
	Runner_Source_Runner_TileBaseDefault_h_17_RPC_WRAPPERS \
	Runner_Source_Runner_TileBaseDefault_h_17_INCLASS \
	Runner_Source_Runner_TileBaseDefault_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Runner_Source_Runner_TileBaseDefault_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_TileBaseDefault_h_17_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_TileBaseDefault_h_17_SPARSE_DATA \
	Runner_Source_Runner_TileBaseDefault_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Runner_Source_Runner_TileBaseDefault_h_17_INCLASS_NO_PURE_DECLS \
	Runner_Source_Runner_TileBaseDefault_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUNNER_API UClass* StaticClass<class ATileBaseDefault>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Runner_Source_Runner_TileBaseDefault_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
