// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUNNER_RunnerCharacter_generated_h
#error "RunnerCharacter.generated.h already included, missing '#pragma once' in RunnerCharacter.h"
#endif
#define RUNNER_RunnerCharacter_generated_h

#define Runner_Source_Runner_RunnerCharacter_h_9_DELEGATE \
static inline void FOnCharacterDeath_DelegateWrapper(const FMulticastScriptDelegate& OnCharacterDeath) \
{ \
	OnCharacterDeath.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Runner_Source_Runner_RunnerCharacter_h_14_SPARSE_DATA
#define Runner_Source_Runner_RunnerCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGoToCorrectLine); \
	DECLARE_FUNCTION(execLineTick); \
	DECLARE_FUNCTION(execMovingRight); \
	DECLARE_FUNCTION(execMovingLeft); \
	DECLARE_FUNCTION(execDeathTickBeforePause); \
	DECLARE_FUNCTION(execStartTick); \
	DECLARE_FUNCTION(execStartRun); \
	DECLARE_FUNCTION(execDeath); \
	DECLARE_FUNCTION(execLower); \
	DECLARE_FUNCTION(execActionsValidCheck); \
	DECLARE_FUNCTION(execRunTick);


#define Runner_Source_Runner_RunnerCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGoToCorrectLine); \
	DECLARE_FUNCTION(execLineTick); \
	DECLARE_FUNCTION(execMovingRight); \
	DECLARE_FUNCTION(execMovingLeft); \
	DECLARE_FUNCTION(execDeathTickBeforePause); \
	DECLARE_FUNCTION(execStartTick); \
	DECLARE_FUNCTION(execStartRun); \
	DECLARE_FUNCTION(execDeath); \
	DECLARE_FUNCTION(execLower); \
	DECLARE_FUNCTION(execActionsValidCheck); \
	DECLARE_FUNCTION(execRunTick);


#define Runner_Source_Runner_RunnerCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunnerCharacter(); \
	friend struct Z_Construct_UClass_ARunnerCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunnerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ARunnerCharacter)


#define Runner_Source_Runner_RunnerCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesARunnerCharacter(); \
	friend struct Z_Construct_UClass_ARunnerCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunnerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ARunnerCharacter)


#define Runner_Source_Runner_RunnerCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunnerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunnerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunnerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunnerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunnerCharacter(ARunnerCharacter&&); \
	NO_API ARunnerCharacter(const ARunnerCharacter&); \
public:


#define Runner_Source_Runner_RunnerCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunnerCharacter(ARunnerCharacter&&); \
	NO_API ARunnerCharacter(const ARunnerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunnerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunnerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunnerCharacter)


#define Runner_Source_Runner_RunnerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RotMove() { return STRUCT_OFFSET(ARunnerCharacter, RotMove); } \
	FORCEINLINE static uint32 __PPO__DeathFXTransform() { return STRUCT_OFFSET(ARunnerCharacter, DeathFXTransform); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ARunnerCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(ARunnerCharacter, FollowCamera); }


#define Runner_Source_Runner_RunnerCharacter_h_11_PROLOG
#define Runner_Source_Runner_RunnerCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_RunnerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_RunnerCharacter_h_14_SPARSE_DATA \
	Runner_Source_Runner_RunnerCharacter_h_14_RPC_WRAPPERS \
	Runner_Source_Runner_RunnerCharacter_h_14_INCLASS \
	Runner_Source_Runner_RunnerCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Runner_Source_Runner_RunnerCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_RunnerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_RunnerCharacter_h_14_SPARSE_DATA \
	Runner_Source_Runner_RunnerCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Runner_Source_Runner_RunnerCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Runner_Source_Runner_RunnerCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUNNER_API UClass* StaticClass<class ARunnerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Runner_Source_Runner_RunnerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
