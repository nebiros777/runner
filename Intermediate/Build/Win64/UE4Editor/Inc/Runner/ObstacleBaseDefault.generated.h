// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef RUNNER_ObstacleBaseDefault_generated_h
#error "ObstacleBaseDefault.generated.h already included, missing '#pragma once' in ObstacleBaseDefault.h"
#endif
#define RUNNER_ObstacleBaseDefault_generated_h

#define Runner_Source_Runner_ObstacleBaseDefault_h_12_SPARSE_DATA
#define Runner_Source_Runner_ObstacleBaseDefault_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTriggerBeginOverlap);


#define Runner_Source_Runner_ObstacleBaseDefault_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTriggerBeginOverlap);


#define Runner_Source_Runner_ObstacleBaseDefault_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAObstacleBaseDefault(); \
	friend struct Z_Construct_UClass_AObstacleBaseDefault_Statics; \
public: \
	DECLARE_CLASS(AObstacleBaseDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(AObstacleBaseDefault)


#define Runner_Source_Runner_ObstacleBaseDefault_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAObstacleBaseDefault(); \
	friend struct Z_Construct_UClass_AObstacleBaseDefault_Statics; \
public: \
	DECLARE_CLASS(AObstacleBaseDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(AObstacleBaseDefault)


#define Runner_Source_Runner_ObstacleBaseDefault_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AObstacleBaseDefault(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AObstacleBaseDefault) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacleBaseDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacleBaseDefault); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacleBaseDefault(AObstacleBaseDefault&&); \
	NO_API AObstacleBaseDefault(const AObstacleBaseDefault&); \
public:


#define Runner_Source_Runner_ObstacleBaseDefault_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacleBaseDefault(AObstacleBaseDefault&&); \
	NO_API AObstacleBaseDefault(const AObstacleBaseDefault&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacleBaseDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacleBaseDefault); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AObstacleBaseDefault)


#define Runner_Source_Runner_ObstacleBaseDefault_h_12_PRIVATE_PROPERTY_OFFSET
#define Runner_Source_Runner_ObstacleBaseDefault_h_9_PROLOG
#define Runner_Source_Runner_ObstacleBaseDefault_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_SPARSE_DATA \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_RPC_WRAPPERS \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_INCLASS \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Runner_Source_Runner_ObstacleBaseDefault_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_SPARSE_DATA \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_INCLASS_NO_PURE_DECLS \
	Runner_Source_Runner_ObstacleBaseDefault_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUNNER_API UClass* StaticClass<class AObstacleBaseDefault>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Runner_Source_Runner_ObstacleBaseDefault_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
