// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef RUNNER_BonusBaseDefault_generated_h
#error "BonusBaseDefault.generated.h already included, missing '#pragma once' in BonusBaseDefault.h"
#endif
#define RUNNER_BonusBaseDefault_generated_h

#define Runner_Source_Runner_BonusBaseDefault_h_12_SPARSE_DATA
#define Runner_Source_Runner_BonusBaseDefault_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTriggerBeginOverlap);


#define Runner_Source_Runner_BonusBaseDefault_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTriggerBeginOverlap);


#define Runner_Source_Runner_BonusBaseDefault_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABonusBaseDefault(); \
	friend struct Z_Construct_UClass_ABonusBaseDefault_Statics; \
public: \
	DECLARE_CLASS(ABonusBaseDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ABonusBaseDefault)


#define Runner_Source_Runner_BonusBaseDefault_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABonusBaseDefault(); \
	friend struct Z_Construct_UClass_ABonusBaseDefault_Statics; \
public: \
	DECLARE_CLASS(ABonusBaseDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ABonusBaseDefault)


#define Runner_Source_Runner_BonusBaseDefault_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusBaseDefault(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABonusBaseDefault) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusBaseDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusBaseDefault); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusBaseDefault(ABonusBaseDefault&&); \
	NO_API ABonusBaseDefault(const ABonusBaseDefault&); \
public:


#define Runner_Source_Runner_BonusBaseDefault_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusBaseDefault(ABonusBaseDefault&&); \
	NO_API ABonusBaseDefault(const ABonusBaseDefault&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusBaseDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusBaseDefault); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABonusBaseDefault)


#define Runner_Source_Runner_BonusBaseDefault_h_12_PRIVATE_PROPERTY_OFFSET
#define Runner_Source_Runner_BonusBaseDefault_h_9_PROLOG
#define Runner_Source_Runner_BonusBaseDefault_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_BonusBaseDefault_h_12_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_BonusBaseDefault_h_12_SPARSE_DATA \
	Runner_Source_Runner_BonusBaseDefault_h_12_RPC_WRAPPERS \
	Runner_Source_Runner_BonusBaseDefault_h_12_INCLASS \
	Runner_Source_Runner_BonusBaseDefault_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Runner_Source_Runner_BonusBaseDefault_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_BonusBaseDefault_h_12_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_BonusBaseDefault_h_12_SPARSE_DATA \
	Runner_Source_Runner_BonusBaseDefault_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Runner_Source_Runner_BonusBaseDefault_h_12_INCLASS_NO_PURE_DECLS \
	Runner_Source_Runner_BonusBaseDefault_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUNNER_API UClass* StaticClass<class ABonusBaseDefault>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Runner_Source_Runner_BonusBaseDefault_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
