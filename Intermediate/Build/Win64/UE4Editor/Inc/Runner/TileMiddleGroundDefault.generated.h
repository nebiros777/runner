// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UStaticMeshComponent;
struct FVector4;
class UInstancedStaticMeshComponent;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef RUNNER_TileMiddleGroundDefault_generated_h
#error "TileMiddleGroundDefault.generated.h already included, missing '#pragma once' in TileMiddleGroundDefault.h"
#endif
#define RUNNER_TileMiddleGroundDefault_generated_h

#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_SPARSE_DATA
#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetFloatsFromBound); \
	DECLARE_FUNCTION(execRandomSpawnDecor); \
	DECLARE_FUNCTION(execSetNextTileIsBuilt); \
	DECLARE_FUNCTION(execTriggerBeginOverlap); \
	DECLARE_FUNCTION(execDestroyTick);


#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetFloatsFromBound); \
	DECLARE_FUNCTION(execRandomSpawnDecor); \
	DECLARE_FUNCTION(execSetNextTileIsBuilt); \
	DECLARE_FUNCTION(execTriggerBeginOverlap); \
	DECLARE_FUNCTION(execDestroyTick);


#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATileMiddleGroundDefault(); \
	friend struct Z_Construct_UClass_ATileMiddleGroundDefault_Statics; \
public: \
	DECLARE_CLASS(ATileMiddleGroundDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ATileMiddleGroundDefault)


#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATileMiddleGroundDefault(); \
	friend struct Z_Construct_UClass_ATileMiddleGroundDefault_Statics; \
public: \
	DECLARE_CLASS(ATileMiddleGroundDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), NO_API) \
	DECLARE_SERIALIZER(ATileMiddleGroundDefault)


#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATileMiddleGroundDefault(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATileMiddleGroundDefault) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATileMiddleGroundDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATileMiddleGroundDefault); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATileMiddleGroundDefault(ATileMiddleGroundDefault&&); \
	NO_API ATileMiddleGroundDefault(const ATileMiddleGroundDefault&); \
public:


#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATileMiddleGroundDefault(ATileMiddleGroundDefault&&); \
	NO_API ATileMiddleGroundDefault(const ATileMiddleGroundDefault&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATileMiddleGroundDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATileMiddleGroundDefault); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATileMiddleGroundDefault)


#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_PRIVATE_PROPERTY_OFFSET
#define Runner_Source_Runner_TileMiddleGroundDefault_h_9_PROLOG
#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_SPARSE_DATA \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_RPC_WRAPPERS \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_INCLASS \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Runner_Source_Runner_TileMiddleGroundDefault_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_SPARSE_DATA \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_INCLASS_NO_PURE_DECLS \
	Runner_Source_Runner_TileMiddleGroundDefault_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUNNER_API UClass* StaticClass<class ATileMiddleGroundDefault>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Runner_Source_Runner_TileMiddleGroundDefault_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
