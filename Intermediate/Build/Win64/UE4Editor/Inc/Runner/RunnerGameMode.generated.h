// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATileMiddleGroundDefault;
class ATileBaseDefault;
#ifdef RUNNER_RunnerGameMode_generated_h
#error "RunnerGameMode.generated.h already included, missing '#pragma once' in RunnerGameMode.h"
#endif
#define RUNNER_RunnerGameMode_generated_h

#define Runner_Source_Runner_RunnerGameMode_h_15_SPARSE_DATA
#define Runner_Source_Runner_RunnerGameMode_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnMiddleGroundTile); \
	DECLARE_FUNCTION(execSpawnTile);


#define Runner_Source_Runner_RunnerGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnMiddleGroundTile); \
	DECLARE_FUNCTION(execSpawnTile);


#define Runner_Source_Runner_RunnerGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunnerGameMode(); \
	friend struct Z_Construct_UClass_ARunnerGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), RUNNER_API) \
	DECLARE_SERIALIZER(ARunnerGameMode)


#define Runner_Source_Runner_RunnerGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARunnerGameMode(); \
	friend struct Z_Construct_UClass_ARunnerGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Runner"), RUNNER_API) \
	DECLARE_SERIALIZER(ARunnerGameMode)


#define Runner_Source_Runner_RunnerGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	RUNNER_API ARunnerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunnerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RUNNER_API, ARunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunnerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RUNNER_API ARunnerGameMode(ARunnerGameMode&&); \
	RUNNER_API ARunnerGameMode(const ARunnerGameMode&); \
public:


#define Runner_Source_Runner_RunnerGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RUNNER_API ARunnerGameMode(ARunnerGameMode&&); \
	RUNNER_API ARunnerGameMode(const ARunnerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RUNNER_API, ARunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunnerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunnerGameMode)


#define Runner_Source_Runner_RunnerGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define Runner_Source_Runner_RunnerGameMode_h_12_PROLOG
#define Runner_Source_Runner_RunnerGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_RunnerGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_RunnerGameMode_h_15_SPARSE_DATA \
	Runner_Source_Runner_RunnerGameMode_h_15_RPC_WRAPPERS \
	Runner_Source_Runner_RunnerGameMode_h_15_INCLASS \
	Runner_Source_Runner_RunnerGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Runner_Source_Runner_RunnerGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Runner_Source_Runner_RunnerGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Runner_Source_Runner_RunnerGameMode_h_15_SPARSE_DATA \
	Runner_Source_Runner_RunnerGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Runner_Source_Runner_RunnerGameMode_h_15_INCLASS_NO_PURE_DECLS \
	Runner_Source_Runner_RunnerGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUNNER_API UClass* StaticClass<class ARunnerGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Runner_Source_Runner_RunnerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
