// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runner/TileMiddleGroundDefault.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTileMiddleGroundDefault() {}
// Cross Module References
	RUNNER_API UClass* Z_Construct_UClass_ATileMiddleGroundDefault_NoRegister();
	RUNNER_API UClass* Z_Construct_UClass_ATileMiddleGroundDefault();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Runner();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector4();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UInstancedStaticMeshComponent_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ATileMiddleGroundDefault::execGetFloatsFromBound)
	{
		P_GET_OBJECT(UStaticMeshComponent,Z_Param_CurrentComponent);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector4*)Z_Param__Result=P_THIS->GetFloatsFromBound(Z_Param_CurrentComponent);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileMiddleGroundDefault::execRandomSpawnDecor)
	{
		P_GET_OBJECT(UStaticMeshComponent,Z_Param_CurrentComponent);
		P_GET_OBJECT(UInstancedStaticMeshComponent,Z_Param_CurrentObject);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RandomSpawnDecor(Z_Param_CurrentComponent,Z_Param_CurrentObject);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileMiddleGroundDefault::execSetNextTileIsBuilt)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetNextTileIsBuilt();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileMiddleGroundDefault::execTriggerBeginOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TriggerBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileMiddleGroundDefault::execDestroyTick)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DestroyTick(Z_Param_DeltaTime);
		P_NATIVE_END;
	}
	void ATileMiddleGroundDefault::StaticRegisterNativesATileMiddleGroundDefault()
	{
		UClass* Class = ATileMiddleGroundDefault::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DestroyTick", &ATileMiddleGroundDefault::execDestroyTick },
			{ "GetFloatsFromBound", &ATileMiddleGroundDefault::execGetFloatsFromBound },
			{ "RandomSpawnDecor", &ATileMiddleGroundDefault::execRandomSpawnDecor },
			{ "SetNextTileIsBuilt", &ATileMiddleGroundDefault::execSetNextTileIsBuilt },
			{ "TriggerBeginOverlap", &ATileMiddleGroundDefault::execTriggerBeginOverlap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics
	{
		struct TileMiddleGroundDefault_eventDestroyTick_Parms
		{
			float DeltaTime;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventDestroyTick_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileMiddleGroundDefault, nullptr, "DestroyTick", nullptr, nullptr, sizeof(TileMiddleGroundDefault_eventDestroyTick_Parms), Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics
	{
		struct TileMiddleGroundDefault_eventGetFloatsFromBound_Parms
		{
			UStaticMeshComponent* CurrentComponent;
			FVector4 ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventGetFloatsFromBound_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::NewProp_CurrentComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::NewProp_CurrentComponent = { "CurrentComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventGetFloatsFromBound_Parms, CurrentComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::NewProp_CurrentComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::NewProp_CurrentComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::NewProp_CurrentComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileMiddleGroundDefault, nullptr, "GetFloatsFromBound", nullptr, nullptr, sizeof(TileMiddleGroundDefault_eventGetFloatsFromBound_Parms), Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics
	{
		struct TileMiddleGroundDefault_eventRandomSpawnDecor_Parms
		{
			UStaticMeshComponent* CurrentComponent;
			UInstancedStaticMeshComponent* CurrentObject;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentObject_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentObject = { "CurrentObject", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventRandomSpawnDecor_Parms, CurrentObject), Z_Construct_UClass_UInstancedStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentComponent = { "CurrentComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventRandomSpawnDecor_Parms, CurrentComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::NewProp_CurrentComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileMiddleGroundDefault, nullptr, "RandomSpawnDecor", nullptr, nullptr, sizeof(TileMiddleGroundDefault_eventRandomSpawnDecor_Parms), Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileMiddleGroundDefault, nullptr, "SetNextTileIsBuilt", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics
	{
		struct TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult_MetaData)) };
	void Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms), &Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileMiddleGroundDefault, nullptr, "TriggerBeginOverlap", nullptr, nullptr, sizeof(TileMiddleGroundDefault_eventTriggerBeginOverlap_Parms), Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATileMiddleGroundDefault_NoRegister()
	{
		return ATileMiddleGroundDefault::StaticClass();
	}
	struct Z_Construct_UClass_ATileMiddleGroundDefault_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MinRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestroyTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DestroyTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestroyTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DestroyTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsNextTileBuilt_MetaData[];
#endif
		static void NewProp_bIsNextTileBuilt_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsNextTileBuilt;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThirdObjectOnTheGround_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThirdObjectOnTheGround;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondObjectOnTheGround_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SecondObjectOnTheGround;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstObjectOnTheGround_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FirstObjectOnTheGround;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Trigger_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Trigger;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Connection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightField_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RightField;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftField_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LeftField;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATileMiddleGroundDefault_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Runner,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATileMiddleGroundDefault_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATileMiddleGroundDefault_DestroyTick, "DestroyTick" }, // 1048369458
		{ &Z_Construct_UFunction_ATileMiddleGroundDefault_GetFloatsFromBound, "GetFloatsFromBound" }, // 978631761
		{ &Z_Construct_UFunction_ATileMiddleGroundDefault_RandomSpawnDecor, "RandomSpawnDecor" }, // 3195942436
		{ &Z_Construct_UFunction_ATileMiddleGroundDefault_SetNextTileIsBuilt, "SetNextTileIsBuilt" }, // 4275178823
		{ &Z_Construct_UFunction_ATileMiddleGroundDefault_TriggerBeginOverlap, "TriggerBeginOverlap" }, // 1122366011
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TileMiddleGroundDefault.h" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MaxRange_MetaData[] = {
		{ "Category", "Enviroment in fields" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MaxRange = { "MaxRange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, MaxRange), METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MaxRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MaxRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MinRange_MetaData[] = {
		{ "Category", "Enviroment in fields" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MinRange = { "MinRange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, MinRange), METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MinRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MinRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTimer_MetaData[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTimer = { "DestroyTimer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, DestroyTimer), METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTimer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTime_MetaData[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTime = { "DestroyTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, DestroyTime), METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_bIsNextTileBuilt_MetaData[] = {
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	void Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_bIsNextTileBuilt_SetBit(void* Obj)
	{
		((ATileMiddleGroundDefault*)Obj)->bIsNextTileBuilt = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_bIsNextTileBuilt = { "bIsNextTileBuilt", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ATileMiddleGroundDefault), &Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_bIsNextTileBuilt_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_bIsNextTileBuilt_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_bIsNextTileBuilt_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_ThirdObjectOnTheGround_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_ThirdObjectOnTheGround = { "ThirdObjectOnTheGround", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, ThirdObjectOnTheGround), Z_Construct_UClass_UInstancedStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_ThirdObjectOnTheGround_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_ThirdObjectOnTheGround_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SecondObjectOnTheGround_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SecondObjectOnTheGround = { "SecondObjectOnTheGround", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, SecondObjectOnTheGround), Z_Construct_UClass_UInstancedStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SecondObjectOnTheGround_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SecondObjectOnTheGround_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_FirstObjectOnTheGround_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_FirstObjectOnTheGround = { "FirstObjectOnTheGround", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, FirstObjectOnTheGround), Z_Construct_UClass_UInstancedStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_FirstObjectOnTheGround_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_FirstObjectOnTheGround_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Trigger_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Trigger = { "Trigger", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, Trigger), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Trigger_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Trigger_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Connection_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Connection = { "Connection", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, Connection), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Connection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Connection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_RightField_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_RightField = { "RightField", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, RightField), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_RightField_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_RightField_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_LeftField_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_LeftField = { "LeftField", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, LeftField), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_LeftField_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_LeftField_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SceneComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileMiddleGroundDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SceneComponent = { "SceneComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileMiddleGroundDefault, SceneComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SceneComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SceneComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATileMiddleGroundDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MaxRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_MinRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_DestroyTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_bIsNextTileBuilt,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_ThirdObjectOnTheGround,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SecondObjectOnTheGround,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_FirstObjectOnTheGround,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Trigger,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_Connection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_RightField,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_LeftField,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileMiddleGroundDefault_Statics::NewProp_SceneComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATileMiddleGroundDefault_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATileMiddleGroundDefault>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATileMiddleGroundDefault_Statics::ClassParams = {
		&ATileMiddleGroundDefault::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATileMiddleGroundDefault_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATileMiddleGroundDefault_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATileMiddleGroundDefault()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATileMiddleGroundDefault_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATileMiddleGroundDefault, 101999154);
	template<> RUNNER_API UClass* StaticClass<ATileMiddleGroundDefault>()
	{
		return ATileMiddleGroundDefault::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATileMiddleGroundDefault(Z_Construct_UClass_ATileMiddleGroundDefault, &ATileMiddleGroundDefault::StaticClass, TEXT("/Script/Runner"), TEXT("ATileMiddleGroundDefault"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATileMiddleGroundDefault);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
