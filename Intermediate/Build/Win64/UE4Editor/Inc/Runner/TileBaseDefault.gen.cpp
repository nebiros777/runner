// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runner/TileBaseDefault.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTileBaseDefault() {}
// Cross Module References
	RUNNER_API UClass* Z_Construct_UClass_ATileBaseDefault_NoRegister();
	RUNNER_API UClass* Z_Construct_UClass_ATileBaseDefault();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Runner();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ATileBaseDefault::execRandomSpawnObstacle)
	{
		P_GET_STRUCT(FTransform,Z_Param_Transform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RandomSpawnObstacle(Z_Param_Transform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileBaseDefault::execSpawnObstacle)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnObstacle();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileBaseDefault::execSetNextTileIsBuilt)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetNextTileIsBuilt();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileBaseDefault::execTriggerBeginOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TriggerBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATileBaseDefault::execDestroyTick)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DestroyTick(Z_Param_DeltaTime);
		P_NATIVE_END;
	}
	void ATileBaseDefault::StaticRegisterNativesATileBaseDefault()
	{
		UClass* Class = ATileBaseDefault::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DestroyTick", &ATileBaseDefault::execDestroyTick },
			{ "RandomSpawnObstacle", &ATileBaseDefault::execRandomSpawnObstacle },
			{ "SetNextTileIsBuilt", &ATileBaseDefault::execSetNextTileIsBuilt },
			{ "SpawnObstacle", &ATileBaseDefault::execSpawnObstacle },
			{ "TriggerBeginOverlap", &ATileBaseDefault::execTriggerBeginOverlap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics
	{
		struct TileBaseDefault_eventDestroyTick_Parms
		{
			float DeltaTime;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileBaseDefault_eventDestroyTick_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileBaseDefault, nullptr, "DestroyTick", nullptr, nullptr, sizeof(TileBaseDefault_eventDestroyTick_Parms), Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileBaseDefault_DestroyTick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileBaseDefault_DestroyTick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics
	{
		struct TileBaseDefault_eventRandomSpawnObstacle_Parms
		{
			FTransform Transform;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileBaseDefault_eventRandomSpawnObstacle_Parms, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::NewProp_Transform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileBaseDefault, nullptr, "RandomSpawnObstacle", nullptr, nullptr, sizeof(TileBaseDefault_eventRandomSpawnObstacle_Parms), Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileBaseDefault, nullptr, "SetNextTileIsBuilt", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileBaseDefault, nullptr, "SpawnObstacle", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics
	{
		struct TileBaseDefault_eventTriggerBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileBaseDefault_eventTriggerBeginOverlap_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult_MetaData)) };
	void Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((TileBaseDefault_eventTriggerBeginOverlap_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TileBaseDefault_eventTriggerBeginOverlap_Parms), &Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileBaseDefault_eventTriggerBeginOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileBaseDefault_eventTriggerBeginOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileBaseDefault_eventTriggerBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TileBaseDefault_eventTriggerBeginOverlap_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_SweepResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::NewProp_OverlappedComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATileBaseDefault, nullptr, "TriggerBeginOverlap", nullptr, nullptr, sizeof(TileBaseDefault_eventTriggerBeginOverlap_Parms), Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATileBaseDefault_NoRegister()
	{
		return ATileBaseDefault::StaticClass();
	}
	struct Z_Construct_UClass_ATileBaseDefault_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Obstacles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Obstacles;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Obstacles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxRandomObstacleID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxRandomObstacleID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestroyTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DestroyTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestroyTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DestroyTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsNextTileBuilt_MetaData[];
#endif
		static void NewProp_bIsNextTileBuilt_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsNextTileBuilt;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Line2_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Line2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Line1_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Line1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Line0_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Line0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Trigger_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Trigger;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Connection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TileFloor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TileFloor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATileBaseDefault_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Runner,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATileBaseDefault_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATileBaseDefault_DestroyTick, "DestroyTick" }, // 2330389513
		{ &Z_Construct_UFunction_ATileBaseDefault_RandomSpawnObstacle, "RandomSpawnObstacle" }, // 3783576069
		{ &Z_Construct_UFunction_ATileBaseDefault_SetNextTileIsBuilt, "SetNextTileIsBuilt" }, // 2069821026
		{ &Z_Construct_UFunction_ATileBaseDefault_SpawnObstacle, "SpawnObstacle" }, // 2869927367
		{ &Z_Construct_UFunction_ATileBaseDefault_TriggerBeginOverlap, "TriggerBeginOverlap" }, // 3025803745
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TileBaseDefault.h" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Obstacles_MetaData[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Obstacles = { "Obstacles", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, Obstacles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Obstacles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Obstacles_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Obstacles_Inner = { "Obstacles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_MaxRandomObstacleID_MetaData[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_MaxRandomObstacleID = { "MaxRandomObstacleID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, MaxRandomObstacleID), METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_MaxRandomObstacleID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_MaxRandomObstacleID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTimer_MetaData[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTimer = { "DestroyTimer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, DestroyTimer), METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTimer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTime_MetaData[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTime = { "DestroyTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, DestroyTime), METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_bIsNextTileBuilt_MetaData[] = {
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	void Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_bIsNextTileBuilt_SetBit(void* Obj)
	{
		((ATileBaseDefault*)Obj)->bIsNextTileBuilt = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_bIsNextTileBuilt = { "bIsNextTileBuilt", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ATileBaseDefault), &Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_bIsNextTileBuilt_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_bIsNextTileBuilt_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_bIsNextTileBuilt_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line2_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line2 = { "Line2", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, Line2), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line1_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line1 = { "Line1", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, Line1), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line0_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line0 = { "Line0", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, Line0), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line0_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Trigger_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Trigger = { "Trigger", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, Trigger), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Trigger_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Trigger_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Connection_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Connection = { "Connection", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, Connection), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Connection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Connection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_TileFloor_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_TileFloor = { "TileFloor", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, TileFloor), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_TileFloor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_TileFloor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_SceneComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TileBaseDefault.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_SceneComponent = { "SceneComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATileBaseDefault, SceneComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_SceneComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_SceneComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATileBaseDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Obstacles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Obstacles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_MaxRandomObstacleID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_DestroyTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_bIsNextTileBuilt,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Line0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Trigger,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_Connection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_TileFloor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATileBaseDefault_Statics::NewProp_SceneComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATileBaseDefault_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATileBaseDefault>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATileBaseDefault_Statics::ClassParams = {
		&ATileBaseDefault::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATileBaseDefault_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATileBaseDefault_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATileBaseDefault_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATileBaseDefault()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATileBaseDefault_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATileBaseDefault, 1333016095);
	template<> RUNNER_API UClass* StaticClass<ATileBaseDefault>()
	{
		return ATileBaseDefault::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATileBaseDefault(Z_Construct_UClass_ATileBaseDefault, &ATileBaseDefault::StaticClass, TEXT("/Script/Runner"), TEXT("ATileBaseDefault"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATileBaseDefault);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
