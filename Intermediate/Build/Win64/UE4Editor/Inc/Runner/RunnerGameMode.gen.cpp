// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runner/RunnerGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunnerGameMode() {}
// Cross Module References
	RUNNER_API UClass* Z_Construct_UClass_ARunnerGameMode_NoRegister();
	RUNNER_API UClass* Z_Construct_UClass_ARunnerGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Runner();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	RUNNER_API UClass* Z_Construct_UClass_ATileMiddleGroundDefault_NoRegister();
	RUNNER_API UClass* Z_Construct_UClass_ATileBaseDefault_NoRegister();
	RUNNER_API UClass* Z_Construct_UClass_ABonusBaseDefault_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	RUNNER_API UClass* Z_Construct_UClass_AObstacleBaseDefault_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	DEFINE_FUNCTION(ARunnerGameMode::execSpawnMiddleGroundTile)
	{
		P_GET_OBJECT(UClass,Z_Param_SpawnClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnMiddleGroundTile(Z_Param_SpawnClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ARunnerGameMode::execSpawnTile)
	{
		P_GET_OBJECT(UClass,Z_Param_SpawnClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnTile(Z_Param_SpawnClass);
		P_NATIVE_END;
	}
	void ARunnerGameMode::StaticRegisterNativesARunnerGameMode()
	{
		UClass* Class = ARunnerGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SpawnMiddleGroundTile", &ARunnerGameMode::execSpawnMiddleGroundTile },
			{ "SpawnTile", &ARunnerGameMode::execSpawnTile },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics
	{
		struct RunnerGameMode_eventSpawnMiddleGroundTile_Parms
		{
			TSubclassOf<ATileMiddleGroundDefault>  SpawnClass;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::NewProp_SpawnClass = { "SpawnClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RunnerGameMode_eventSpawnMiddleGroundTile_Parms, SpawnClass), Z_Construct_UClass_ATileMiddleGroundDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::NewProp_SpawnClass,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARunnerGameMode, nullptr, "SpawnMiddleGroundTile", nullptr, nullptr, sizeof(RunnerGameMode_eventSpawnMiddleGroundTile_Parms), Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics
	{
		struct RunnerGameMode_eventSpawnTile_Parms
		{
			TSubclassOf<ATileBaseDefault>  SpawnClass;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::NewProp_SpawnClass = { "SpawnClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RunnerGameMode_eventSpawnTile_Parms, SpawnClass), Z_Construct_UClass_ATileBaseDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::NewProp_SpawnClass,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARunnerGameMode, nullptr, "SpawnTile", nullptr, nullptr, sizeof(RunnerGameMode_eventSpawnTile_Parms), Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARunnerGameMode_SpawnTile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARunnerGameMode_SpawnTile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARunnerGameMode_NoRegister()
	{
		return ARunnerGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ARunnerGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Coins_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Coins;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointsMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PointsMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentPoints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bonus_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Bonus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusGetFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BonusGetFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Obstacle2_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Obstacle2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Obstacle1_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Obstacle1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Obstacle0_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Obstacle0;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InClassMiddleGround_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InClassMiddleGround;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InClassGate_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InClassGate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InClassFirst_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InClassFirst;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MiddleGroundSpawnRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MiddleGroundSpawnRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MiddleGroundSpawnLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MiddleGroundSpawnLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpawnRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpawnLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARunnerGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Runner,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARunnerGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARunnerGameMode_SpawnMiddleGroundTile, "SpawnMiddleGroundTile" }, // 1209108483
		{ &Z_Construct_UFunction_ARunnerGameMode_SpawnTile, "SpawnTile" }, // 1225522852
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "RunnerGameMode.h" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Coins_MetaData[] = {
		{ "Category", "Points" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Coins = { "Coins", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, Coins), METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Coins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Coins_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_PointsMultiplier_MetaData[] = {
		{ "Category", "Points" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_PointsMultiplier = { "PointsMultiplier", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, PointsMultiplier), METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_PointsMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_PointsMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_CurrentPoints_MetaData[] = {
		{ "Category", "Points" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_CurrentPoints = { "CurrentPoints", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, CurrentPoints), METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_CurrentPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_CurrentPoints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Bonus_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Bonus = { "Bonus", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, Bonus), Z_Construct_UClass_ABonusBaseDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Bonus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Bonus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_BonusGetFX_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_BonusGetFX = { "BonusGetFX", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, BonusGetFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_BonusGetFX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_BonusGetFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle2_MetaData[] = {
		{ "Category", "Obstacle" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle2 = { "Obstacle2", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, Obstacle2), Z_Construct_UClass_AObstacleBaseDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle1_MetaData[] = {
		{ "Category", "Obstacle" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle1 = { "Obstacle1", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, Obstacle1), Z_Construct_UClass_AObstacleBaseDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle0_MetaData[] = {
		{ "Category", "Obstacle" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle0 = { "Obstacle0", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, Obstacle0), Z_Construct_UClass_AObstacleBaseDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle0_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle0_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassMiddleGround_MetaData[] = {
		{ "Category", "Tile" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassMiddleGround = { "InClassMiddleGround", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, InClassMiddleGround), Z_Construct_UClass_ATileMiddleGroundDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassMiddleGround_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassMiddleGround_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassGate_MetaData[] = {
		{ "Category", "Tile" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassGate = { "InClassGate", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, InClassGate), Z_Construct_UClass_ATileBaseDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassGate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassGate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassFirst_MetaData[] = {
		{ "Category", "Tile" },
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassFirst = { "InClassFirst", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, InClassFirst), Z_Construct_UClass_ATileBaseDefault_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassFirst_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassFirst_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnRotation_MetaData[] = {
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnRotation = { "MiddleGroundSpawnRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, MiddleGroundSpawnRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnLocation_MetaData[] = {
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnLocation = { "MiddleGroundSpawnLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, MiddleGroundSpawnLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnRotation_MetaData[] = {
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnRotation = { "SpawnRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, SpawnRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnLocation_MetaData[] = {
		{ "ModuleRelativePath", "RunnerGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnLocation = { "SpawnLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunnerGameMode, SpawnLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnLocation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARunnerGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Coins,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_PointsMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_CurrentPoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Bonus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_BonusGetFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_Obstacle0,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassMiddleGround,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassGate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_InClassFirst,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_MiddleGroundSpawnLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunnerGameMode_Statics::NewProp_SpawnLocation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARunnerGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARunnerGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARunnerGameMode_Statics::ClassParams = {
		&ARunnerGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ARunnerGameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::PropPointers),
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ARunnerGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARunnerGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARunnerGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARunnerGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARunnerGameMode, 3079895004);
	template<> RUNNER_API UClass* StaticClass<ARunnerGameMode>()
	{
		return ARunnerGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARunnerGameMode(Z_Construct_UClass_ARunnerGameMode, &ARunnerGameMode::StaticClass, TEXT("/Script/Runner"), TEXT("ARunnerGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARunnerGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
